<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'paris_wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'C}@58#X{D4qaF554k&&yPPwA&^TR@HNN1Tz$;(lK#=ky3a;v0W21TgC+B%p%|Q$J' );
define( 'SECURE_AUTH_KEY',  'Op`C`PE&K{|p/;sSY/m$GW+qC(_M8jQ3&5A=r8Y@D`,FK3O7=S0WC>QgiRH6%@cz' );
define( 'LOGGED_IN_KEY',    'rxZc*>f^*[A|kdJz=PVVkC<xAtzNF:gE%Xc> m77C<m(ZWqF}?m^v6Y(2R9aWmwC' );
define( 'NONCE_KEY',        'SXFSn=R}5jD%(1,eZO^!R~%Xzx%q;i/3GlV&,r8%sZN(xP6>wv&)ga-`X*DWU0(,' );
define( 'AUTH_SALT',        '%QE#[vMH366*)r&U3^bD_}rc|mJ^RxkUK=&I@C0iqLYxvydQMze^C#m[]GIh+!J&' );
define( 'SECURE_AUTH_SALT', '=zfr3&(0oG.=eY/G^3N>A6JOkOc~|~#A}mZ_4zy@?|=/+vn nu6 (Kk.qzx1Jm)H' );
define( 'LOGGED_IN_SALT',   '&Cwln{7qjDX<1VF[Uvb-Cm7x}$JMP$;m)HEy$<t2bIbiTeIQA4|]X*pCcOY0(du>' );
define( 'NONCE_SALT',       '=Oyip3m2V$1T%74 ]{OJ=FIed|Zm$q~@Vh]tuWWA1E=]douPoL`.qd894j5I%5kN' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
