var civi="";
function Civilite() {

    if (document.getElementById('Mr').checked) {
        civi = document.getElementById('Mr').value;
        document.cookie="Civilité="+document.getElementById('Mr').value;
        console.log(civi);
    }
    if(document.getElementById('Mme').checked){
        civi = document.getElementById('Mme').value;
        document.cookie="Civilité="+document.getElementById('Mme').value;
        console.log(civi);
    }
}
function Name() {
    var nom = document.getElementById("nom").value;
    var regex = /[!@$%^&*()_+|~=`{}\[\]:";'<>?,.\/]/;
    nom = nom.toUpperCase();
    document.getElementById("nom").value = nom;
    var ok = true ;
    for (var i=0;i < nom.length;i++) {
        if (!isNaN(nom[i])) {
            ok = false;
            break;
        }
    }
    if(document.getElementById("nom").value==""){
        ok = false;
    }
    if(regex.test(document.getElementById("nom").value)){
        ok = false;
    }
    if (ok == false) {
        document.getElementById("nom").className = "form-control is-invalid";
        document.getElementById("n").innerHTML = " Nom non valide";
    }
    else
    {
        document.getElementById("nom").className = "form-control is-valid";
        document.getElementById("n").innerHTML = "";
    }
    return nom;
}
function Surname() {
    var prenom = document.getElementById("prenom").value;
    var regex = /[@!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]/;
    var maj = prenom.charAt(0).toUpperCase();
    prenom = maj.concat(prenom.substring(1,25));
    document.getElementById("prenom").value = prenom;
    var ok = true ;
    for (var i=0;i < prenom.length;i++) {
        if (!isNaN(prenom[i])) {
            ok = false;
            break;
        }
    }
    if(document.getElementById("prenom").value==""){
        ok = false;
    }
    if(regex.test(document.getElementById("prenom").value)){
        ok = false;
    }

    if (ok == false) {
        document.getElementById("prenom").className = "form-control is-invalid";
        document.getElementById("p").innerHTML = " Prénom non valide";
    }
    else
    {
        document.getElementById("prenom").className = "form-control is-valid";
        document.getElementById("p").innerHTML = "";
    }
}
function Adresse() {
    var adresse = document.getElementById("adresse").value;
    var ville = document.getElementById("ville").value;
    var regex = /[@!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]/;
    var num =parseInt(adresse.substring(0,3));
    var cp = document.getElementById("codep").value;
    var ok = true ;
    for (var i=0;i < ville.length;i++) {
        if (!isNaN(ville[i])) {
            ok = false;
            break;
        }
    }

    if(isNaN(num)||isNaN(cp)||cp.length != 5 || ok==false || regex.test(ville)){
        document.getElementById("adresse").className="form-control is-invalid";
        document.getElementById("ville").className="form-control is-invalid";
        document.getElementById("codep").className="form-control is-invalid";
        document.getElementById("add").innerHTML = " Adresse non valide";

    }else{
        document.getElementById("adresse").className = "form-control is-valid";
        document.getElementById("ville").className = "form-control is-valid";
        document.getElementById("codep").className = "form-control is-valid";
        document.getElementById("add").innerHTML = "";
    }
}
function Mail() {
    var mail = document.getElementById("mail").value;
    if ((mail.indexOf("@")>=0)&&(mail.indexOf(".")>=0)) {
        document.getElementById("mail").className = "form-control is-valid";
        document.getElementById("m").innerHTML="";
    } else {
        document.getElementById("mail").className="form-control is-invalid";
        document.getElementById("m").innerHTML=" Entrez un email valide";
    }
}
function Age() {
    var age = document.getElementById("age").value;
        if(isNaN(age)||age==""){
            document.getElementById("age").className="form-control is-invalid";
            document.getElementById("ag").innerHTML=" Age non valide";
        }else{
            document.getElementById("age").className="form-control is-valid";
            document.getElementById("ag").innerHTML="";
        }
}
function verif_p1(ev){
    var n = document.getElementById("n").innerHTML;
    var p = document.getElementById("p").innerHTML;
    var add = document.getElementById("add").innerHTML;
    var m = document.getElementById("m").innerHTML;
    var a = document.getElementById("ag").innerHTML;

    ev.preventDefault();
    if(n||p||add||m||a != ""){
        document.getElementById("sub").innerHTML=" <p class=\"spacer\">&nbsp;</p>Veuillez remplir les champs correctement";
        return false;
    }
    if (civi == ""){
        document.getElementById("sub").style="margin-left: 265px";
        document.getElementById("sub").innerHTML=" <p class=\"spacer\">&nbsp;</p>Veuillez choisir une civilité";
        return false;
    }
    else{

        document.cookie="nom="+document.getElementById("nom").value;
        document.cookie="prenom="+document.getElementById("prenom").value;
        document.cookie="adresse="+document.getElementById("adresse").value;
        document.cookie="ville="+document.getElementById("ville").value;
        document.cookie="cp="+document.getElementById("codep").value;
        document.cookie="mail="+document.getElementById("mail").value;
        document.cookie="age="+document.getElementById("age").value;
        location.href="page2.html";
        return true;
    }
}

var score_p1 =0;
function resp_check() {

    if((document.getElementById('oui1').checked)&&(document.getElementById('oui2').checked)){
         score_p1 = 20;
    }
    if((document.getElementById('oui1').checked)||(document.getElementById('oui2').checked)){
        point = 10;
        if(score_p1 < 20){
        score_p1 = score_p1 + point;
        }
    }
    if ((document.getElementById('non1').checked)||(document.getElementById('non2').checked)){
        point = 0;
        if(score_p1 <=20 && score_p1 != 10 && score_p1 >0){
            score_p1 = score_p1 - 10;
    }
    return score_p1;
}
}

function verif_p2() {
    if ((document.getElementById('non1').checked==false)&&(document.getElementById('oui1').checked==false)&&
        (document.getElementById('non2').checked==false)&&(document.getElementById('oui2').checked==false))
    {
        document.getElementById("sub2").innerHTML="     Veuillez choisir au moins une reponse";

        return false;
    }else{
        document.cookie="score1="+score_p1;
        return true;

    }
}
function resp_select() {

    var val1 = document.getElementById("select1").value;
    var val2 = document.getElementById("select2").value;
    var score_p2 =( parseInt(document.cookie.replace(/(?:(?:^|.*;\s*)score1\s*\=\s*([^;]*).*$)|^.*$/, "$1"))+parseInt(val1)+parseInt(val2));
    if(val1==0||val2==0){
        document.getElementById("sub3").innerHTML="     Vous avez oublié de repondre a une question";
        return false;
    }else{
        document.getElementById("sub3").innerHTML="";
        document.cookie="score2="+score_p2;
        return true;
    }
}


function nombre() {
    var nb = document.getElementById("nbr").value;
    if(isNaN(nb)||nb==""){
        document.getElementById("nbr").className = "form-control is-invalid";
        document.getElementById("nbr_span").innerHTML = "Entrer un nombre";
        }else{
        document.getElementById("nbr").className = "form-control is-valid";
        document.getElementById("nbr_span").innerHTML="";
    }
    var nombre = parseInt(document.getElementById("nbr").value);
    if(nombre>=6 && nombre < 20 ){
        var score_p3 =( parseInt(document.cookie.replace(/(?:(?:^|.*;\s*)score2\s*\=\s*([^;]*).*$)|^.*$/, "$1"))+5);
    }
    if (nombre>=1 && nombre < 6){
        var score_p3 =( parseInt(document.cookie.replace(/(?:(?:^|.*;\s*)score2\s*\=\s*([^;]*).*$)|^.*$/, "$1"))+2);
    }
    if(nombre==0){
        score_p3 =( parseInt(document.cookie.replace(/(?:(?:^|.*;\s*)score2\s*\=\s*([^;]*).*$)|^.*$/, "$1")));
    }
    if(nombre >=20){
        score_p3 =( parseInt(document.cookie.replace(/(?:(?:^|.*;\s*)score2\s*\=\s*([^;]*).*$)|^.*$/, "$1"))+10);
    }
    document.cookie="score="+score_p3;
}
function date_ver() {
    var nbf=0;
    var date = document.getElementById("date").value;
    var jour= date.substring(0,2);
    var mois= date.substring(3,5);
    var annee= date.substring(6,10);
    var ladate = new Date();
    var j = parseInt(jour);
    var m = parseInt(mois);
    var a = parseInt(annee);
    var ok = true;
    if(date.length!=10){
        document.getElementById("date").className = "form-control is-invalid";
        document.getElementById("d").innerHTML = "Date non valide";
    }else{
        document.getElementById("date").className = "form-control is-valid";
        document.getElementById("d").innerHTML="";
    }
    if(m > 12 || m == 0|| date==""){
        document.getElementById("date").className = "form-control is-invalid";
        document.getElementById("d").innerHTML = "Date non valide";
    }else{
        document.getElementById("date").className = "form-control is-valid";
        document.getElementById("d").innerHTML="";
    }
    if(parseInt(ladate.getMonth()+1)== m && parseInt(ladate.getDate())<j || parseInt(ladate.getMonth()+1)< m  || parseInt(ladate.getFullYear())<a ){
        ok = false;
    }
    if((a%4 ==0 && a%100 !=0)|| a%400==0){
        nbf=29;
    }else{
        nbf=28;
    }
    switch (m){
        case 1:case 3:case 5:case 7:case 8:case 10:case 12:
        if(j<0 ||j>31 || ok == false){
            document.getElementById("date").className = "form-control is-invalid";
            document.getElementById("d").innerHTML = "Date non valide";
        }else{
            document.getElementById("date").className = "form-control is-valid";
            document.getElementById("d").innerHTML="";
        }
        break;
        case 4:case 6:case 9:case 11:
        if(j<0 ||j>30 || ok == false){
            document.getElementById("date").className = "form-control is-invalid";
            document.getElementById("d").innerHTML = "Date non valide";
        }else{
            document.getElementById("date").className = "form-control is-valid";
            document.getElementById("d").innerHTML="";
        }
        break;
        case 2:
        if (j<0||j>nbf || ok == false){
            document.getElementById("date").className = "form-control is-invalid";
            document.getElementById("d").innerHTML = "Date non valide";
        }else{
            document.getElementById("date").className = "form-control is-valid";
            document.getElementById("d").innerHTML="";
        }
        break;
    }
}

function verif_p3() {
    var d = document.getElementById("d").innerHTML;
    var nb_span = document.getElementById("nbr_span").innerHTML;
    if(nb_span||d != ""){
        return false;
    }else{
        return true;
    }
}

var score = parseInt( document.cookie.replace(/(?:(?:^|.*;\s*)score\s*\=\s*([^;]*).*$)|^.*$/, "$1"));
var nom = document.cookie.replace(/(?:(?:^|.*;\s*)nom\s*\=\s*([^;]*).*$)|^.*$/, "$1");
var prenom = document.cookie.replace(/(?:(?:^|.*;\s*)prenom\s*\=\s*([^;]*).*$)|^.*$/, "$1");
var adresse = document.cookie.replace(/(?:(?:^|.*;\s*)adresse\s*\=\s*([^;]*).*$)|^.*$/, "$1");
var ville = document.cookie.replace(/(?:(?:^|.*;\s*)ville\s*\=\s*([^;]*).*$)|^.*$/, "$1");
var codep = document.cookie.replace(/(?:(?:^|.*;\s*)cp\s*\=\s*([^;]*).*$)|^.*$/, "$1");
var mail = document.cookie.replace(/(?:(?:^|.*;\s*)prenom\s*\=\s*([^;]*).*$)|^.*$/, "$1");
var age = document.cookie.replace(/(?:(?:^|.*;\s*)prenom\s*\=\s*([^;]*).*$)|^.*$/, "$1");
var civ = document.cookie.replace(/(?:(?:^|.*;\s*)Civilité\s*\=\s*([^;]*).*$)|^.*$/, "$1");

function satisf(){
    if(score >= 30) {
        document.getElementById("alert_res").className="alert alert-success col-12 p_res";
        document.getElementById("h5_res").innerHTML="d’après notre enquête vous êtes très satisfait";
        document.getElementById("icon").innerHTML="<img src=\"img/icons8-happy-64.png\">";
    }
    if(score>=20 && score<30){
        document.getElementById("alert_res").className="alert alert-primary col-12 p_res";
        document.getElementById("h5_res").innerHTML="d’après notre enquête vous êtes assez satisfait";
        document.getElementById("icon").innerHTML="<img src=\"img/icons8-neutral-64.png\">";
    }
    if(score<=19){
        document.getElementById("alert_res").className="alert alert-danger col-12 p_res";
        document.getElementById("h5_res").innerHTML="d’après notre enquête vous n’êtes pas satisfait";
        document.getElementById("icon").innerHTML="<img src=\"img/icons8-sad-64.png\">";
    }
}
document.getElementById("civ_res").innerHTML=civ;
document.getElementById("nom_res").innerHTML=nom;
function alertCookie() {
    alert(document.cookie);
}