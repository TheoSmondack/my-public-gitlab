<%@ page import="controller.*" %>
<%@ page import="model.user" %><%--
  Created by IntelliJ IDEA.
  User: Théo
  Date: 04/03/2020
  Time: 14:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>S'enregistrer</title>
</head>
<body>
<center>
    <h1>S'enregistrer</h1>
    <form method="post" action="register.jsp">
        Votre identifiant : <input type="text" name="login" id="login"><br><br>
        Nom de l'élève : <input type="text" name="nom" id="nom"><br><br>
        Prénom de l'élève : <input type="text" name="prenom" id="prenom"><br><br>
        Mot de passe : <input type="password" name="password" id="password"><br><br>
        <input type="submit" name="register" value="S'enregistrer">
        <input type="button" name="login" onclick="location.href='login.jsp'" value="Se connecter">
        <input type="button" name="home" onclick="location.href='index.jsp'" value="Acceuil">
    </form>




<%
    if (request.getParameter("register") != null)
    {
        if((!request.getParameter("login").equals(""))&&(!request.getParameter("nom").equals(""))&&(!request.getParameter("prenom").equals(""))&&
                (!request.getParameter("password").equals(""))){
            String login = request.getParameter("login");
            String nom = request.getParameter("nom");
            String prenom = request.getParameter("prenom");
            String password = request.getParameter("password");
            User unUser = new User(login,nom,prenom,password);
//                  Insertion de ce compte dans la BDD
            UserController.createUser(unUser);
            out.print("Insertion dans la base de données réussie ");
        }else {
            out.print("Veuillez remplir tout les champs");
        }

    }
%>
</center>

</body>
</html>
