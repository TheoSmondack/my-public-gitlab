<%@ page import="controller.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="static java.lang.System.out" %>
<%@ page import="static java.lang.System.out" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="static java.lang.System.*" %>
<%@ page import="java.lang.reflect.Array" %>
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.ParseException" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>
  <%@include file="header.jsp"%>
</head>
<body>
<div class="container">
  <h1 style="margin-top: 5%; margin-bottom: 3%">Feuille d'appel</h1>
  <form action="index.jsp" method="post">
    <div class="form-row">
      <div class="form-group col">
        <%
          ArrayList<Ecole> lesEcoles = EcoleController.selectAllEcoles();
          out.print("  <label for=\"sel1\">Etablissement:</label>\n" +
                  "  <select name='sel1' class=\"form-control\" id=\"sel1\"onChange=\"getNiveau(this.value)\">\n" +
                  " <option value='0' id='0'>Choisissez une école</option>");
          for (Ecole uneEcole : lesEcoles) {
            out.print("<option value=" + uneEcole.getId() + " id=" + uneEcole.getId() +">" + uneEcole.getLibelle() + "</option>");
          }
          out.print("</select>");
        %>
      </div>
      <div class="form-group col">
        <%
          out.print("  <label for=\"sel2\">Niveaux:</label>\n" +
                  "  <select class=\"form-control\" id=\"sel2\"onChange=\"getClasse(this.value)\">\n" +
                  " <option value='0'>Choisissez un niveau</option>");
          if (request.getParameter("id_ecole")!= null) {
            int id_ecole = Integer.parseInt(request.getParameter("id_ecole"));
            ArrayList<Niveau> lesNiveaux = NiveauController.selectNiveauxEcole(id_ecole);
            for (Niveau unNiveau : lesNiveaux) {
              out.print("<option value=" + unNiveau.getId() + ">" + unNiveau.getLibelle() + "</option>");
            }
          }
          out.print("</select>");
        %>
      </div>
      <div class="form-group col">
        <%
          out.print("  <label for=\"sel3\">Classe:</label>\n" +
                  "  <select class=\"form-control\" id=\"sel3\">\n" +
                  " <option value='0'>Choisissez une classe</option>");
          if (request.getParameter("id_ecole")!= null && request.getParameter("id_niveau")!= null ) {
            int id_ecole = Integer.parseInt(request.getParameter("id_ecole"));
            int id_niveau = Integer.parseInt(request.getParameter("id_niveau"));

            ArrayList<Classe> lesClasses = ClasseController.selectClasseNiveauEcole(id_ecole,id_niveau);
            for (Classe uneClasse : lesClasses) {
              out.print("<option value=" + uneClasse.getId() + ">" + uneClasse.getLibelle() + "</option>");
            }
          }
          out.print("</select>");
        %>
      </div>
    </div>
    <div class="form-row">
      <div class="form-group col">
        <label>Date de l'appel :</label><bR>
        <%
          Date date = new Date();

          SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
          if(request.getParameter("date_appel")!=null){
            Date select = null;
            try {
              select = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("date_appel"));
            } catch (ParseException e) {
              e.printStackTrace();
            }
            if(select.after(date)){
              out.print("<input type=\"date\" name=\"date_appel\" id=\"date_appel\" value="+ft.format(date)+" " +
                      "max="+ft.format(date)+">");
            }else{
              out.print("<input type=\"date\" name=\"date_appel\" id=\"date_appel\" value="+
                      request.getParameter("date_appel")+" max="+ft.format(date)+">");
            }
          } else{
            out.print("<input type=\"date\" name=\"date_appel\" id=\"date_appel\" value="+ft.format(date)+" " +
                    "max="+ft.format(date)+">");
          }
        %>
      </div>
      <div class="form-group col d-flex justify-content-end">
        <button type="button" class="btn btn-primary btn-lg search_button"
                id="search_button" onclick="getNiveauClasse((document.getElementById('sel3').value),
               (document.getElementById('date_appel').value))">Rechercher
        </button>
      </div>
    </div>
    <div id="eleve_table" <%
      if ((request.getParameter("id_classe") != null)&&(request.getParameter("date_appel") != null)){
        out.print("style=\"display: block;\"");
      }else{
        out.print("style=\"display: none;\"");
      }
    %>>
      <div class="wrap-table100" style="margin-left: -1.8rem;">
        <div class="table100 ver1 m-b-110">
          <div class="table100-head">
            <table>
              <thead>
              <tr class="row100 head">
                <th scope="col" style="width: 28%" class="cell100 column1">Nom</th>
                <th scope="col" style="width: 18%" class="cell100 column2">Prénom</th>
                <th scope="col" style="width: 65%" class="cell100 column3">Présence</th>
              </tr>
              </thead>
            </table>
          </div>

          <div class="table100-body js-pscroll">
            <table>
              <tbody>
              <%
                if (request.getParameter("id_ecole") != null && request.getParameter("id_classe") != null
                        && request.getParameter("date_appel") != null) {
                  int id_ecole = Integer.parseInt(request.getParameter("id_ecole"));
                  int id_classe = Integer.parseInt(request.getParameter("id_classe"));
                  String date_appel = request.getParameter("date_appel");
                  ArrayList<Eleve> LesEleves = EleveController.selectClasseEleves(id_ecole, id_classe,date_appel);
                  String checkedP ="", checkedR ="", checkedA ="";
                  out.print("<form action=\"index.jsp\" method=\"post\">");
                  for (Eleve unEleve : LesEleves) {
                    checkedP =checkedR =checkedA ="";
                    session.setAttribute("eleveID", unEleve.getId_eleve());
                    if (unEleve.getPresence().equals("Présent")) {
                      checkedP = "checked";
                    } else if (unEleve.getPresence().equals("Retard")) {
                      checkedR = "checked";
                    } else if (unEleve.getPresence().equals("Absent")) {
                      checkedA = "checked";
                    }
                    out.print("<tr class=\"row100 body\"  name=\"eleve\" value=" + unEleve.getId_eleve() + "><td class=\"cell100 column1\">" + unEleve.getNom_eleve() + "</td>" +
                            "<td class=\"cell100 column3\" >" + unEleve.getPrenom_eleve() + "</td>" +
                            "" +
                            "<input type='hidden' name='ideleve[]' value ='"+ unEleve.getId_eleve()+"'>"+
                            "<td class=\"cell100 column3 presences\">" +
                            "<div class=\"btn-group btn-group-toggle ml-auto mr-auto\" data-toggle=\"buttons\">\n" +
                            "  <label class=\"btn btn-outline-success button_pres_ret\">\n" +
                            "    <input type=\"radio\" name=\"options"+unEleve.getId_eleve()+"\"  value=\"1\" " + checkedP + " > Présent\n" +
                            "  </label>\n" +
                            "  <label class=\"btn btn-outline-warning button_pres_ret\">\n" +
                            "    <input type=\"radio\" name=\"options"+unEleve.getId_eleve()+"\"  value=\"2\" " + checkedR + " > Retard\n" +
                            "  </label>\n" +
                            "  <label class=\"btn btn-outline-danger button_abs\">\n" +
                            "    <input type=\"radio\" name=\"options"+unEleve.getId_eleve()+"\"  value=\"3\" " + checkedA + " > Absent\n" +
                            "  </label>\n" +
                            "</div>" +
                            "</td>" +

                            "</tr>");
                  }
                }

              %>

              </tbody>
            </table>
          </div>
        </div>
      </div>
      <input class="btn btn-primary btn-lg search_button" style="margin-top: -10%;" type="submit" name="actualiser" id="appel_button"  value="Appel Terminé">
      <%
        if (request.getParameter("actualiser")!= null){
          String date_appel = request.getParameter("date_appel");
          String tabEleves []= request.getParameterValues("ideleve[]");
          int idOptions = 0, idEleve =0;
          for (int i = 0; i< tabEleves.length; i++)
          {
            idEleve = Integer.parseInt(tabEleves[i]);
            idOptions = Integer.parseInt(request.getParameter("options"+tabEleves[i]));
            PresenceController.updatePresence(idEleve,date_appel, idOptions );
          }
        }
      %>
    </div>
  </form>
</div>
<%@ include file="footer.jsp"%>
</body>
</html>
