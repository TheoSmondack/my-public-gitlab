-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 17 fév. 2020 à 23:13
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gestion_ecole`
--

-- --------------------------------------------------------

--
-- Structure de la table `classe`
--

DROP TABLE IF EXISTS `classe`;
CREATE TABLE IF NOT EXISTS `classe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) NOT NULL,
  `id_niveau` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classe_niveau_id_fk` (`id_niveau`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `classe`
--

INSERT INTO `classe` (`id`, `libelle`, `id_niveau`) VALUES
(1, 'CP 1', 1),
(2, 'CP 2', 1),
(3, 'CE1 1', 2),
(4, 'CE1 2', 2),
(5, 'CE2 1', 3),
(6, 'CE2 2', 3),
(7, 'CM1 1', 4),
(8, 'CM1 2', 4),
(9, 'CM2 1', 5),
(10, 'CM2 2', 5),
(11, '6 ème A', 6),
(12, '6 ème B', 6),
(13, '6 ème C', 6),
(14, '5 ème A', 7),
(15, '5 ème B', 7),
(16, '5 ème C', 7),
(17, '4 ème A', 8),
(18, '4 ème B', 8),
(19, '4 ème C', 8),
(20, '3 ème A', 9),
(21, '3 ème B', 9),
(22, '3 ème C', 9);

-- --------------------------------------------------------

--
-- Structure de la table `ecole`
--

DROP TABLE IF EXISTS `ecole`;
CREATE TABLE IF NOT EXISTS `ecole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ecole`
--

INSERT INTO `ecole` (`id`, `libelle`) VALUES
(1, 'Ecole Primaire 1'),
(2, 'Ecole Primaire 2'),
(3, 'Collège 1'),
(4, 'Collège 2'),
(5, 'Lycée 1');

-- --------------------------------------------------------

--
-- Structure de la table `ecole_classe`
--

DROP TABLE IF EXISTS `ecole_classe`;
CREATE TABLE IF NOT EXISTS `ecole_classe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_ecole` int(11) NOT NULL,
  `id_classe` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ecole_classe`
--

INSERT INTO `ecole_classe` (`id`, `id_ecole`, `id_classe`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 5),
(5, 1, 6),
(6, 1, 7),
(7, 1, 8),
(8, 1, 9),
(9, 1, 10),
(10, 2, 1),
(11, 2, 3),
(12, 2, 4),
(13, 2, 5),
(14, 2, 7),
(15, 2, 9),
(16, 2, 10),
(17, 3, 11),
(18, 3, 12),
(19, 3, 13),
(20, 3, 14),
(21, 3, 15),
(22, 3, 17),
(23, 3, 18),
(24, 3, 20),
(25, 3, 21),
(26, 3, 22);

-- --------------------------------------------------------

--
-- Structure de la table `ecole_niveau`
--

DROP TABLE IF EXISTS `ecole_niveau`;
CREATE TABLE IF NOT EXISTS `ecole_niveau` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_ecole` int(11) NOT NULL,
  `id_niveau` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ecole_niveau_niveau_id_fk` (`id_niveau`),
  KEY `ecole_niveau_ecole_id_fk` (`id_ecole`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ecole_niveau`
--

INSERT INTO `ecole_niveau` (`id`, `id_ecole`, `id_niveau`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 2, 1),
(7, 2, 2),
(8, 2, 3),
(9, 2, 4),
(10, 2, 5),
(11, 3, 6),
(12, 3, 7),
(13, 3, 8),
(14, 3, 9);

-- --------------------------------------------------------

--
-- Structure de la table `eleve`
--

DROP TABLE IF EXISTS `eleve`;
CREATE TABLE IF NOT EXISTS `eleve` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_eleve` varchar(55) DEFAULT NULL,
  `prenom_eleve` varchar(55) DEFAULT NULL,
  `id_ecole` int(11) NOT NULL,
  `id_classe` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `eleve`
--

INSERT INTO `eleve` (`id`, `nom_eleve`, `prenom_eleve`, `id_ecole`, `id_classe`) VALUES
(1, 'LeTest', 'PrenomTest', 1, 2),
(2, 'Bonjour', 'Test', 2, 1),
(3, 'NomTest', 'PrenomTest', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `niveau`
--

DROP TABLE IF EXISTS `niveau`;
CREATE TABLE IF NOT EXISTS `niveau` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `niveau`
--

INSERT INTO `niveau` (`id`, `libelle`) VALUES
(1, 'Cours préparatoire'),
(2, 'Cours élémentaire 1re année '),
(3, 'Cours élémentaire 2e année'),
(4, 'Cours moyen 1re année'),
(5, 'Cours moyen 2e année'),
(6, '6 ème'),
(7, '5 ème'),
(8, '4 ème'),
(9, '3 ème');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
