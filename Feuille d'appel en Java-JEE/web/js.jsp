<script>
    function getNiveau(val) {
        $.ajax({
            type:"POST",
            url:"index.jsp",
            async:true,
            data:{id_ecole:val},
            success:function (){
                if(val == 0){
                    window.location.href="index.jsp";
                }else{
                    window.location.href="index.jsp?id_ecole="+val;
                }
            }
        })
    }
    function getClasse(val) {
        var id_ecole = $("#sel1").val();
        $.ajax({
            type:"POST",
            url:"index.jsp",
            data:'id_niveau='+val,
            success:function (){
                if(val != 0){
                    window.location.href="index.jsp?id_ecole="+id_ecole+"&id_niveau="+val;
                }
            }
        })
    }
    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }
    function getNiveauClasse(val,d_app) {
        var id_ecole = $("#sel1").val();
        var id_niveau = $("#sel2").val();
        var today = new Date();
        var todayFormat = formatDate(today);
        var selectedDate = document.getElementById("date_appel").value;
        selectedDate = new Date(selectedDate);
        $.ajax({
            type:"POST",
            url:"index.jsp",
            data:{id_classe: val, date_appel: d_app},
            success:function (){
                if(val != 0){
                    window.location.href="index.jsp?id_ecole="+id_ecole+"&id_niveau="+id_niveau+"&id_classe="+val+"&date_appel="+d_app;
                }
                if((val != 0)&&(selectedDate > today)){
                    window.location.href="index.jsp?id_ecole="+id_ecole+"&id_niveau="+id_niveau+"&id_classe="+val+"&date_appel="+todayFormat;
                }
            }
        })
    }


    $(function() {
        if ((localStorage.getItem('sel1')) && (window.location.href.indexOf("id_ecole") > -1)) {
            $("#sel1 option").eq(localStorage.getItem('sel1')).prop('selected', true);
        }

        $("#sel1").on('change', function() {
                localStorage.setItem('sel1', $('option:selected', this).index());
                $('#sel2').val('0').change();
                $('#sel3').val('0').change();
        });

    });
    $(function() {
        if ((localStorage.getItem('sel2'))) {
            $("#sel2 option").eq(localStorage.getItem('sel2')).prop('selected', true);
        }

        $("#sel2").on('change', function() {
            localStorage.setItem('sel2', $('option:selected', this).index());
            $('#sel3').val('0').change();
        });

    });
    $(function() {
        if (localStorage.getItem('sel3')) {
            $("#sel3 option").eq(localStorage.getItem('sel3')).prop('selected', true);
        }

        $("#sel3").on('change', function() {
            localStorage.setItem('sel3', $('option:selected', this).index());
        });

    });







</script>
