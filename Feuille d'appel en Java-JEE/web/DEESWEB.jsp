<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %><%--
  Created by IntelliJ IDEA.
  User: Théo
  Date: 09/04/2020
  Time: 11:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    String typeSejour = request.getParameter("type");
    if (typeSejour.equals("linguisitique")||typeSejour.equals("thematique")){
        return true;
    }else {
        return false;
    }
    try {
        String datedebVerif = request.getParameter("datedeb");
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        df.setLenient(false);
        java.util.Date date = df.parse(datedebVerif);
        System.out.printLn(df.format(date));
    }catch (Exception e){
        e.printStackTrace();
    }
    try {
        String datefinVerif = request.getParameter("datefin");
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        df.setLenient(false);
        java.util.Date date = df.parse(datefinVerif);
        System.out.printLn(df.format(date));
    }catch (Exception e){
        e.printStackTrace();
    }
%>
</body>
</html>
