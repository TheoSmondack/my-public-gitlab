package controller;

public class Eleve {
    private int id_eleve;
    private String nom_eleve;
    private String prenom_eleve;
    private int id_ecole;
    private int id_classe;
    private String presence;

    public Eleve(int id_eleve, String nom_eleve, String prenom_eleve, int id_ecole, int id_classe, String presence) {
        this.id_eleve = id_eleve;
        this.nom_eleve = nom_eleve;
        this.prenom_eleve = prenom_eleve;
        this.id_ecole = id_ecole;
        this.id_classe = id_classe;
        this.presence = presence;
    }

    public int getId_eleve() {
        return id_eleve;
    }

    public void setId_eleve(int id_eleve) {
        this.id_eleve = id_eleve;
    }

    public String getNom_eleve() {
        return nom_eleve;
    }

    public void setNom_eleve(String nom_eleve) {
        this.nom_eleve = nom_eleve;
    }

    public String getPrenom_eleve() {
        return prenom_eleve;
    }

    public void setPrenom_eleve(String prenom_eleve) {
        this.prenom_eleve = prenom_eleve;
    }

    public int getId_ecole() {
        return id_ecole;
    }

    public void setId_ecole(int id_ecole) {
        this.id_ecole = id_ecole;
    }

    public int getId_classe() {
        return id_classe;
    }

    public void setId_classe(int id_classe) {
        this.id_classe = id_classe;
    }

    public String getPresence() {
        return presence;
    }

    public void setPresence(String presence) {
        this.presence = presence;
    }
}
