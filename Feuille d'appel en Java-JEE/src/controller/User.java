package controller;

public class User {
    private String login;
    private String nom;
    private String prenom;
    private String password;

    public User(String login, String nom, String prenom, String password) {
        super();
        this.login = login;
        this.nom = nom;
        this.prenom = prenom;
        this.password = password;
    }


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
