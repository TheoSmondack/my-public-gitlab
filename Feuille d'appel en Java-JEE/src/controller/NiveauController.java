package controller;

import model.niveau;

import java.util.ArrayList;

public class NiveauController {

    public static ArrayList<Niveau> selectNiveauxEcole(int ecoleID){

        return niveau.selectNiveauxEcole(ecoleID);
    }
}
