package controller;
import model.user;

public class UserController {
    public  static void createUser (User unUser){
        user.createUser(unUser);
    }
    public  static User connectUser (String login, String password){
        return user.connectUser(login,password);
    }

}
