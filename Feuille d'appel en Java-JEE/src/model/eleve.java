package model;

import controller.Eleve;
import controller.PresenceController;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class eleve {
    private static BDD uneBdd = new BDD("localhost","gestion_ecole","root","");
    public static ArrayList<Eleve> selectClasseEleves(int id_ecole,int id_classe,String date_appel){
        String requete1 ="insert into eleve_presence (date_appel, id_eleve, id_presence)\n" +
                "select distinct '"+date_appel+"', eleve.id_eleve,1 from eleve\n" +
                "left outer join eleve_presence on (eleve.id_eleve = eleve_presence.id_eleve)\n" +
                "where not exists(\n" +
                "    select date_appel from eleve_presence\n" +
                "    where date_appel = '"+date_appel+"' and id_eleve <> eleve.id_eleve\n" +
                ");\n";
        ArrayList<Eleve> lesEleves = new ArrayList<Eleve>();
        String requete2 ="select * from eleve\n" +
                "inner join classe on eleve.id_classe = classe.id\n" +
                "inner join ecole on eleve.id_ecole = ecole.id\n" +
                "inner join eleve_presence on eleve.id_eleve = eleve_presence.id_eleve\n" +
                "inner join presence on eleve_presence.id_presence = presence.id\n" +
                "where id_ecole = "+id_ecole+" and  id_classe = "+id_classe+" and date_appel='"+date_appel+"'\n" +
                "order by nom_eleve;";
        try{
            uneBdd.seConnecter();
            Statement unStat = uneBdd.getMaConnexion().createStatement();
            unStat.execute(requete1);
            ResultSet desRes = unStat.executeQuery(requete2);
            while (desRes.next()){
                Eleve unEleve = new Eleve(
                        desRes.getInt("id_eleve"),
                        desRes.getString("nom_eleve"),
                        desRes.getString("prenom_eleve"),
                        desRes.getInt("id_ecole"),
                        desRes.getInt("id_classe"),
                        desRes.getString("presence.libelle")
                );

                lesEleves.add(unEleve);
            }
            uneBdd.seDeconnecter();
        }catch (SQLException exp){
            exp.printStackTrace();
        }
        return lesEleves;
    }
}