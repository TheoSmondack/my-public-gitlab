package model;

import controller.Classe;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class classe {
    private static BDD uneBdd = new BDD("localhost","gestion_ecole","root","");
    public static ArrayList<Classe> selectClasseNiveauEcole(int ecoleID,int niveauID){
        ArrayList<Classe> lesClasses = new ArrayList<Classe>();
        String requete = "select * from classe\n" +
                "inner join niveau on classe.id_niveau = niveau.id\n" +
                "inner join ecole_classe on classe.id=ecole_classe.id_classe\n" +
                "where niveau.id="+niveauID+" and ecole_classe.id_ecole="+ecoleID+";";
        try{
            uneBdd.seConnecter();
            Statement unStat = uneBdd.getMaConnexion().createStatement();
            ResultSet desRes = unStat.executeQuery(requete);
            while (desRes.next()){
                Classe uneClasse = new Classe(
                        desRes.getInt("id"),
                        desRes.getString("libelle")
                );
                lesClasses.add(uneClasse);
            }
            uneBdd.seDeconnecter();
        }catch (SQLException exp){
            exp.printStackTrace();
        }
        return lesClasses;
    }

}
