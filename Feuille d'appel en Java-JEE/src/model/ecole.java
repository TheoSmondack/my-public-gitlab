package model;

import controller.Ecole;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ecole {
    private static BDD uneBdd = new BDD("localhost","gestion_ecole","root","");
    public static ArrayList<Ecole> selectAllEcoles(){
        ArrayList<Ecole> lesEcoles = new ArrayList<Ecole>();
        String requete ="select * from ecole;";
        try{
            uneBdd.seConnecter();
            Statement unStat = uneBdd.getMaConnexion().createStatement();
            ResultSet desRes = unStat.executeQuery(requete);
            while (desRes.next()){
                Ecole uneEcole = new Ecole(desRes.getInt("id"),
                    desRes.getString("libelle")
                );
                lesEcoles.add(uneEcole);
            }
            uneBdd.seDeconnecter();
        }catch (SQLException exp){
            exp.printStackTrace();
        }
        return lesEcoles;
    }
}
