package model;

import controller.Ecole;
import controller.Niveau;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class niveau {
    private static BDD uneBdd = new BDD("localhost","gestion_ecole","root","");
    public static ArrayList<Niveau> selectNiveauxEcole(int ecoleID){
        ArrayList<Niveau> lesNiveaux = new ArrayList<Niveau>();
        String requete = "select * from niveau\n" +
                "inner join ecole_niveau on niveau.id = ecole_niveau.id_niveau\n" +
                "inner join ecole on ecole_niveau.id_ecole = ecole.id\n" +
                "where ecole.id="+ecoleID+";";
        try{
            uneBdd.seConnecter();
            Statement unStat = uneBdd.getMaConnexion().createStatement();
            ResultSet desRes = unStat.executeQuery(requete);
            while (desRes.next()){
                Niveau unNiveau = new Niveau(
                        desRes.getInt("id"),
                        desRes.getString("libelle")
                );
                lesNiveaux.add(unNiveau);
            }
            uneBdd.seDeconnecter();
        }catch (SQLException exp){
            exp.printStackTrace();
        }
        return lesNiveaux;
    }
}
