$(document).ready(function () {
    //Changement d'apparence de la navbar lorsqu'on scroll
    $(window).scroll(function () {
        if($(this).scrollTop()>511){
            $("#nav_logo").attr('src','img/logo_parisJobs_white.png');
            $("#login_logo").attr('src','img/icon/icons8-signin-50_dark.png');
            $("#nav_1").attr('class','navbar fixed-top navbar-expand-lg navbar-dark bg-primary');

        }else{
            $("#nav_logo").attr('src','img/logo_parisJobs_black.png');
            $("#login_logo").attr('src','img/icon/icons8-signin-50_dark.png');
            $("#nav_1").attr('class','navbar fixed-top navbar-expand-lg navbar-light bg-light');
        }

    });
    //Affichage du modal de connexion
    $('.login_mod').on('click', function(e){
        e.preventDefault();
        $('#login_modal').modal('show').find('.modal-content').load($(this).attr('href'));
    });
    //Affichage du modal d'inscription
    $(function(){

        $('#register').click(function() {
            $('#register_modal').modal();
            $('body').addClass('modal-open');
        });
        $(document).on('submit', '#formRegister', function(e) {
            e.preventDefault();

            $('input+small').text('');
            $('input').parent().removeClass('has-error');

            $.ajax({
                method: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json"
            })
                .done(function(data) {
                    $('.alert-success').removeClass('hidden');
                    $('#register_modal').modal('hide');
                })
                .fail(function(data) {
                    $.each(data.responseJSON, function (key, value) {
                        var input = '#formRegister input[name=' + key + ']';
                        $(input + '+small').text(value);
                        $(input).parent().addClass('has-error');
                    });
                });
        });

    });
    $
    //Empeche la multiplication de la div class = "modal-backdrop"
    $(".modal").on("shown.bs.modal", function () {
        if ($(".modal-backdrop").length > 1) {
            $(".modal-backdrop").not(':first').remove();
        }
    });

   // $( "#reg_content" ).load("/register");


});
