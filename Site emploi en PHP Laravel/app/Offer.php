<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable = ['poste','entreprise','ville_ent','departement',
        'type_contrat','desc','remuneration'];

    public function users(){
        return $this->belongsToMany('App\User');
    }
    public function applications(){
        return $this->belongsToMany('App\Application');
    }

}
