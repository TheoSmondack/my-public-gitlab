<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    public function users(){
        return $this->belongsToMany('App\User');
    }
    public function offers(){
        return $this->belongsToMany('App\Offer');
    }


}
