<?php

namespace App\Http\Controllers\Job;

use App\Application;
use App\Http\Controllers\Controller;
use App\Offer;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Gate;


class ApplicationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offers = Offer::all();
        $user = Auth::user();
        $applications =  Application::all();
        return view('job.applications.index')->with('applications',$applications)->with('user',$user)->with('offers',$offers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $offer = request('offer_id');
        $user = Auth::user()->getAuthIdentifier();
        if(Gate::denies('apply-users')){
            return redirect('/cannotapply');
        }

        $application = Application::create();
        $application-> users()->attach($user);
        $application-> offers()->attach($offer);



        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function show(Application $application)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function edit(Application $application)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Application $application)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function destroy(Application $application)
    {
        //
    }
    public function CandidatsList(Offer $offer){
        $users = User::all();
        $applications =  Application::all();
        return view('job.offers.candidats')->with('offer',$offer)->with('users',$users)
            ->with('applications',$applications);

    }

}
