<?php

namespace App\Http\Controllers\Job;

use App\Http\Controllers\Controller;
use App\Offer;
use App\User;
use App\Application;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OffersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $offers = Offer::all();
        return view('job.offers.index')->with('offers',$offers)->with('user',$user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('job.offers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
            'poste' => 'required',
            'entreprise' => 'required',
            'ville_ent' => 'required',
            'departement' => 'required|min:2|max:3',
            'type_contrat' => 'required',
            'desc' => 'required',
            'remuneration' => 'required',
        ));
        $offer = Offer::create([
            'poste' => request('poste'),
            'entreprise' => request('entreprise'),
            'ville_ent' => request('ville_ent'),
            'departement' => request('departement'),
            'type_contrat' => request('type_contrat'),
            'desc' => request('desc'),
            'remuneration' => request('remuneration'),
        ]);
        $user = Auth::user()->getAuthIdentifier();
        $offer-> users()->attach($user);


        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function show(Offer $offer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function edit(Offer $offer)
    {
        return view('job.offers.edit')->with('offer',$offer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offer $offer)
    {
        $offer->poste = $request->poste;
        $offer->entreprise = $request->entreprise;
        $offer->ville_ent = $request->ville_ent;
        $offer->departement = $request->departement;
        $offer->desc = $request->desc;
        $offer->type_contrat = $request->type_contrat;
        $offer->remuneration = $request->remuneration;
        $offer->save();
        return redirect() -> route('job.offers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offer $offer)
    {
        $offer->users()->detach();
        $offer->applications()->detach();
        $offer->delete();
        return redirect()->route('job.offers.index');
    }

}
