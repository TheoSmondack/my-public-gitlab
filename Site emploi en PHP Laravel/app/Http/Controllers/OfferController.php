<?php

namespace App\Http\Controllers;

use App\Offer;
use App\User;
use Illuminate\Http\Request;

class OfferController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        $offers = Offer::all();
        return (view('offer',compact('offers')));
    }
    public function store(){
        $offer = request()->validate([
            'poste' => 'required',
            'entreprise' => 'required',
            'ville_ent' => 'required',
            'departement' => 'required|min:2|max:3',
            'type_contrat' => 'required',
            'desc' => 'required',
            'remuneration' => 'required',
        ]);

        $user = User::select('id')->where('prenom','Super')->first();
        $offer-> users()->attach($user);

        Offer::create($offer);
        return redirect('/');
    }
}
