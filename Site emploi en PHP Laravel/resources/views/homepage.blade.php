<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">
    <title>Paris Jobs</title>
</head>
<body>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
<!----------------------------------------------------------------------------------------------------------Navbar--------------------------------------------------------------------------------------------------------------------->
<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light" id="nav_1">
    <a class="navbar-brand" href="#">
        <img id="nav_logo" src="img/logo_parisJobs_black.png" width="106" height="40" alt="">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="http://localhost:63342/Projet_BACHELOR_Web/index.html">Mairie de Paris</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/service">Services</a>
            </li>
        </ul>
    </div>
    <a href="/login" class='login_mod'>
        <img id="login_logo" src="img/icon/icons8-signin-50_dark.png" width="auto" height="30" alt=""align="right">
    </a>
</nav>
<!----------------------------------------------------------------------------------------------------------Entête-------------------------------------------------------------------------------------------------------------------->

<div class="jumbotron" style=" background-image: url({{ URL::asset('img/back_jumbotron.jpg') }});">
    <h1 class="display-4" style="color: white">Bienvenue sur le site de la <br> mairie de Paris</h1><br><br>
    <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
</div>
<!----------------------------------------------------------------------------------------------------------Login Modal---------------------------------------------------------------------------------------------------------------->
<div id="login_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>
<!----------------------------------------------------------------------------------------------------------Register Modal---------------------------------------------------------------------------------------------------------------->
<div id="register_modal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="reg_content">
        </div>
    </div>
</div>
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
<section></section>
<section></section>
<script type="text/javascript" src="{{ asset('js/script.js') }}"></script>

</body>
</html>