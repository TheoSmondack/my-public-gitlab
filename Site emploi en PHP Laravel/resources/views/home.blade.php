@extends('layouts.app')

@section('content')
    <div class="wrapper">
        <div class="sidebar">
            <h2>Recherche</h2>
            <ul style="padding-inline-start: unset;">
                <li>
                    <input type="text" class="form-control" placeholder="&#xF002;" style="font-family:Nunito, sans-serif,FontAwesome">
                </li>
            </ul>
        </div>
    </div>
        <div class="main_content">
            <div class="row justify-content-center">
                @foreach($offers as $offer)
                    <div class="col-md-8 border-top">
                        <h3 class="annonce-title">{{$offer->poste}}</h3>
                        <h5 style="font-style: italic;">{{$offer->entreprise}} - {{$offer->ville_ent}} ({{$offer->departement}})</h5>
                        <h5 class="text-primary">{{$offer->type_contrat}}</h5>
                        <p>{{$offer->desc}}</p>
                        <p style="font-weight: bold">{{$offer->remuneration}}</p>
                        <form action="{{route('job.applications.store')}}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-primary btn-lg marge-bot" name="offer_id" value="{{$offer->id}}">Postuler</button>
                        </form>


                    </div>
                @endforeach
            </div>
        </div>

@endsection
