<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Se connecter</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="form-group">
            <label for="email_login">Identifiant</label>
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required>
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message}}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group">
            <label for="password">Mot de passe</label>
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
            @error('password')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{$message}}</strong>
                                    </span>
            @enderror
        </div>
        <div class="form-group">
                <button type="submit" class="btn btn-outline-primary btn-lg btn-block col-6" style="margin-top: auto;">Se connecter</button>
        </div>
    </form>
    <a href="#register_modal" class="btn btn-outline-primary btn-lg btn-block col-6" data-toggle="modal" data-dismiss="modal" id="register_mod">S'enregistrer</a>
</div>

