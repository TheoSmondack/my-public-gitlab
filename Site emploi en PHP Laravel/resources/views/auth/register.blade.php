
            <div class="modal-header" id="register_head">
                <h5 class="modal-title" id="exampleModalLabel">S'enregistrer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="form-row" id="register_gender">
                        <label id="label_register_gender">Vous etes : </label>
                        <div class="btn-group btn-group-toggle form-group" data-toggle="buttons">
                            <label class="btn btn-outline-primary">
                                <input type="radio" name="civi" id="Mr" value="Monsieur"> Monsieur
                            </label>
                            <label class="btn btn-outline-primary">
                                <input type="radio" name="civi" id="Mme" value="Madame" autocomplete="off"> Madame
                            </label>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="register_name">Nom</label>
                            <input type="text" class="form-control" id="register_name" name="name" placeholder="Entrez votre nom">
                        </div>
                        <div class="form-group col">
                            <label for="register_surname">Prénom</label>
                            <input type="text" class="form-control" id="register_surname" name="prenom" placeholder="Entrez votre prénom">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-7">
                            <label for="register_adress">Adresse</label>
                            <input type="text" id="register_adress" class="form-control" name="adresse" placeholder="Entrez votre adresse">
                        </div>
                        <div class="form-group col">
                            <label for="register_city">&nbsp; </label>
                            <input type="text" id="register_city" class="form-control" name="ville" placeholder="Ville">
                        </div>
                        <div class="form-group col">
                            <label for="register_zip">&nbsp; </label>
                            <input type="text" id="register_zip" class="form-control" name="codep" placeholder="Code Postal">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="register_date">Date de naissance</label>
                        <input type="date" id="register_date" class="form-control" name="dateNaiss" placeholder="Entrez votre date de naissance">
                    </div>
                    <div class="form-group">
                        <label for="register_mail">Email</label>
                        <input type="email" id="register_mail" class="form-control" name="email" placeholder="Entrez un email de connexion">
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="register_pass">Mot de passe</label>
                            <input type="password" id="register_pass" class="form-control" name="password" placeholder="Entrez un mot de passe">
                        </div>
                        <div class="form-group col">
                            <label for="register_pass_ver">&nbsp;</label>
                            <input type="password" id="register_pass_ver" class="form-control" name="password_confirmation" placeholder="Retaper le mot de passe">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-outline-primary btn-lg btn-block">
                        S'enregistrer
                    </button>
                </form>
            </div>
            <div class="modal-footer">
                <a href="#"  class="btn btn-outline-primary btn-lg btn-block">S'enregistrer</a>
                <a href="#login_modal" data-toggle="modal" data-dismiss="modal" class="btn btn-outline-primary btn-lg btn-block">
                    Se connecter
                </a>
            </div>
