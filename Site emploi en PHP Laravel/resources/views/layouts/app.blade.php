<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Paris Jobs</title>

    <!-- Scripts -->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/8b5d2b73aa.js" crossorigin="anonymous"></script>

    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">


    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light fixed-top bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{asset('img/logo_parisJobs_black.png')}}"  height="50"/>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a href="{{ route('login') }}" class='login_mod'>
                                    <img id="login_logo" src="img/icon/icons8-signin-50_dark.png" width="auto" height="40" alt=""align="right">
                                </a>
                                <!--<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>-->
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->prenom }} {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Déconnexion
                                    </a>
                                    <a class="dropdown-item" href="/logged">
                                        Mon Profil
                                    </a>
                                    @can('manage-users')
                                    <a class="dropdown-item" href="{{route('job.offers.create')}}">
                                        Créer une offre d'emploi
                                    </a>
                                    @endcan
                                    @can('manage-users')
                                        <a class="dropdown-item" href="{{route('job.offers.index')}}">
                                            Consulter vos offres d'emploi
                                        </a>
                                    @endcan
                                    @can('apply-users')
                                        <a class="dropdown-item" href="{{route('job.applications.index')}}">
                                            Mes candidatures
                                        </a>
                                    @endcan
                                    @can('edit-users')
                                    <a class="dropdown-item" href={{route('admin.users.index')}}>
                                        Gestion utilisateurs
                                    </a>
                                    @endcan

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <!----------------------------------------------------------------------------------------------------------Login Modal---------------------------------------------------------------------------------------------------------------->
        <div id="login_modal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                </div>
            </div>
        </div>
        <main class="py-4">
            @yield('content')
        </main>
        <div id="register_modal" class="modal fade">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" id="reg_content">
                    <div class="modal-header" id="register_head">
                        <h5 class="modal-title" id="exampleModalLabel">S'enregistrer</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="form-row" id="register_gender">
                                <label id="label_register_gender">Vous etes : </label>
                                <div class="btn-group btn-group-toggle form-group" data-toggle="buttons">
                                    <label class="btn btn-outline-primary">
                                        <input type="radio"  name="civi" value="Monsieur" {{ (old('civi') == 'Monsieur')}}/>Monsieur
                                    </label>
                                    <label class="btn btn-outline-primary">
                                        <input type="radio"  name="civi" value="Madame" {{ (old('civi') == 'Madame')}}/>Madame
                                    </label>
                                </div>
                                @error('civi')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="name">Nom</label>
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name"
                                           @if ($errors->has('name')) autofocus @endif placeholder="Entrez votre nom"/>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group col">
                                    <label for="prenom">Prénom</label>
                                    <input id="prenom" type="text" class="form-control @error('prenom') is-invalid @enderror" name="prenom" value="{{ old('prenom') }}"
                                           required autocomplete="prenom" autofocus placeholder="Entrez votre nom" />
                                    @error('prenom')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-7">
                                    <label for="adresse">Adresse</label>
                                    <input id="adresse" type="text" class="form-control @error('adresse') is-invalid @enderror" name="adresse" value="{{ old('adresse') }}"
                                           required autocomplete="adresse" placeholder="Entrez votre adresse">
                                    @error('adresse')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group col">
                                    <label for="ville">&nbsp; </label>
                                    <input id="ville" type="text" class="form-control @error('ville') is-invalid @enderror" name="ville" value="{{ old('ville') }}"
                                           required autocomplete="ville" placeholder="Ville">
                                    @error('ville')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group col">
                                    <label for="codep">&nbsp; </label>
                                    <input id="codep" type="text" class="form-control @error('codep') is-invalid @enderror" name="codep" value="{{ old('codep') }}"
                                           required autocomplete="codep" placeholder="Code Postal">
                                    @error('codep')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dateNaiss">Date de naissance</label>
                                <input id="dateNaiss" type="date" class="form-control @error('dateNaiss') is-invalid @enderror" name="dateNaiss" value="{{ old('dateNaiss') }}" required autocomplete="dateNaiss"
                                       placeholder="Entrez votre date de naissance">
                                @error('dateNaiss')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email"
                                       placeholder="Entrez un email de connexion">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="password">Mot de passe</label>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password"
                                           placeholder="Entrez un mot de passe">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group col">
                                    <label for="password-confirm">&nbsp;</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Retaper le mot de passe">
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" id="register_button" class="btn btn-outline-primary btn-lg btn-block col-6" style="margin-top: auto;">S'enregistrer</button>
                            </div>
                        </form>
                        <a href="#login_modal" class="btn btn-outline-primary btn-lg btn-block col-6" data-toggle="modal" data-dismiss="modal" id="register_mod">Se connecter</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('js/script.js') }}"></script>
</body>
</html>
