@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="alert alert-danger" role="alert">
                    Nous sommes désolés vous ne pouvez pas postuler à cette offre si vous n’êtes pas connectés en tant que candidat.
                   <br> Connecter vous ou enregistrez vous en cliquant sur le logo de connexion en haut à droite.

                </div>
            </div>
        </div>
    </div>


@endsection