@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="alert alert-danger" role="alert">
                    Vous devez être connectés pour accéder à cette page veuillez retourner à l'accueil en cliquant <a href="/">ici</a> ou
                    vous connectez en cliquant sur le logo de connexion en haut à droite de la barre de navigation.
                </div>
            </div>
        </div>
    </div>
@endsection
