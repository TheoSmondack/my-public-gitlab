@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Utilisateurs</div>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">Prenom</th>
                                    <th scope="col">Nom</th>
                                    <th scope="col">email</th>
                                    <th scope="col">Type d'utilisateur</th>
                                    @can('edit-users')
                                    <th scope="col">Action</th>
                                        @endcan
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->prenom}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{implode(',',$user->roles()->get()->pluck('name')->toArray())}}</td>
                                        <td>
                                            @can('edit-users')
                                            <a href="{{route('admin.users.edit',$user->id)}}" ><button type="button" class="btn btn-primary float-left" style="margin-right: 3px;">Editer</button></a>
                                            @endcan
                                            @can('delete-users')
                                            <form action="{{route('admin.users.destroy',$user)}}" method="POST" class="float-left" >
                                                @csrf
                                                {{method_field('DELETE')}}
                                                <button type="submit" class="btn btn-danger">Supprimer</button>
                                            </form>
                                                @endcan
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection