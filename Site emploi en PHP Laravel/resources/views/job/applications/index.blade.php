@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Vos candidatures</div>
                    <div class="card-body" style="margin-left: 13px;">
                        <div class="row">
                            <ul class="list-group list-group-flush col" style="padding-right: 3px;">
                                <li class="list-group-item active">Poste</li>
                                @foreach($applications as $application)
                                    @if($application->users()->get()->pluck('id')->contains($user->id))
                                        <li class="list-group-item">{{ $application->offers()->pluck('poste')->implode(",<br>") }}</li>
                                    @endif
                                @endforeach
                            </ul>
                            <ul class="list-group list-group-flush col" style="padding-right: 3px;">
                                <li class="list-group-item active">Entreprise</li>
                                @foreach($applications as $application)
                                    @if($application->users()->get()->pluck('id')->contains($user->id))
                                        <li class="list-group-item">{{ $application->offers()->pluck('entreprise')->implode(",<br>") }}</li>
                                    @endif
                                @endforeach
                            </ul>
                            <ul class="list-group list-group-flush col">
                                <li class="list-group-item active">Date de la candidature</li>
                                @foreach($applications as $application)
                                    @if($application->users()->get()->pluck('id')->contains($user->id))
                                        <li class="list-group-item">{{$application->created_at}}</li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>


@endsection