@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Vos offres</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Poste</th>
                                <th scope="col">Entreprise</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>

                                @foreach($offers as $offer)
                                    @if($offer->users()->get()->pluck('id')->contains($user->id))
                                    <tr>
                                        <td>{{$offer->poste}}</td>
                                        <td>{{$offer->entreprise}}</td>
                                        <td>
                                            <a href="{{route('job.offers.edit',$offer->id)}}" ><button type="button" class="btn btn-primary float-left" style="margin-right: 3px;">Editer</button></a>
                                            <a href="{{route('job.',$offer->id)}}" ><button type="button" class="btn btn-success float-left" style="margin-right: 3px;">Candidats</button></a>
                                            <form action="{{route('job.offers.destroy',$offer)}}" method="POST" class="float-left" >
                                                @csrf
                                                {{method_field('DELETE')}}
                                                <button type="submit" class="btn btn-danger">Supprimer</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endif
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection