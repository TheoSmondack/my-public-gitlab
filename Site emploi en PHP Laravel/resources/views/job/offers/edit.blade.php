@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Editer {{$offer->poste}}</div>
                    <div class="card-body">
                        <form method="POST" action="{{route('job.offers.update',$offer)}}">
                            @csrf
                            {{method_field('PUT')}}
                            <div class="form-group">
                                <label for="poste">Intitulé du poste</label>
                                <input id="poste" type="text" class="form-control @error('poste') is-invalid @enderror" name="poste" value="{{$offer->poste}}" required>
                                @error('poste')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="entreprise">Entreprise</label>
                                <input id="entreprise" type="text" class="form-control @error('entreprise') is-invalid @enderror" name="entreprise" value="{{$offer->entreprise}}" required>
                                @error('entreprise')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-row">
                                <div class="form-group col-10">
                                    <label for="ville_ent">Localisation</label>
                                    <input id="ville_ent" type="text" class="form-control @error('ville_ent') is-invalid @enderror" name="ville_ent" value="{{ $offer->ville_ent}}" placeholder="Ville" required>
                                    @error('ville_ent')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group col-2">
                                    <label for="departement">&nbsp;</label>
                                    <input id="departement" type="text" class="form-control @error('departement') is-invalid @enderror" name="departement" value="{{ $offer->departement}}" placeholder="Département" required>
                                    @error('departement')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group">
                                    <label for="desc">Description du poste</label>
                                    <textarea class="form-control" style="width: 692px" rows="10" id="desc" class="form-control @error('desc') is-invalid @enderror" name="desc" value="{{ $offer->desc }}" required>{{ $offer->desc }}</textarea>
                                    @error('desc')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="type_contrat">Type de contrat</label>
                                    <input class="form-control" type="text" id="type_contrat" class="form-control @error('type_contrat') is-invalid @enderror" name="type_contrat" value="{{ $offer->type_contrat}}" required>
                                    @error('type_contrat')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group col">
                                    <label for="remuneration">Rémuneration</label>
                                    <input class="form-control" type="text" id="remuneration" class="form-control @error('remuneration') is-invalid @enderror" name="remuneration" value="{{ $offer->remuneration }}" required>
                                    @error('remuneration')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-outline-primary btn-lg" style="margin-left: 253px">Mettre à jour l'offre</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection