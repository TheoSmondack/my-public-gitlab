@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Liste des candidats de {{$offer->poste}}</div>
                    <div class="card-body" style="margin-left: 13px;">
                        <div class="row">
                            <ul class="list-group list-group-flush col" style="padding-right: 3px;">
                                <li class="list-group-item active">Nom</li>
                                @foreach($applications as $application)
                                    @if($application->offers()->get()->pluck('id')->contains($offer->id))
                                        <li class="list-group-item">{{implode($application->users()->get()->pluck('name')->toArray(),',')}}</li>
                                    @endif
                                @endforeach
                            </ul>
                            <ul class="list-group list-group-flush col" style="padding-right: 3px;">
                                <li class="list-group-item active">Prénom</li>
                                @foreach($applications as $application)
                                    @if($application->offers()->get()->pluck('id')->contains($offer->id))

                                        <li class="list-group-item">{{implode($application->users()->get()->pluck('prenom')->toArray(),',')}}</li>
                                    @endif
                                @endforeach
                            </ul>
                            <ul class="list-group list-group-flush col" style="padding-right: 3px;">
                                <li class="list-group-item active">Email</li>
                                @foreach($applications as $application)
                                    @if($application->offers()->get()->pluck('id')->contains($offer->id))
                                        <li class="list-group-item">{{implode($application->users()->get()->pluck('email')->toArray(),'<br>')}}</li>
                                    @endif
                                @endforeach
                            </ul>
                            <ul class="list-group list-group-flush col">
                                <li class="list-group-item active">Date de la candidature</li>
                                @foreach($applications as $application)
                                    @if($application->offers()->get()->pluck('id')->contains($offer->id))
                                        <li class="list-group-item">{{$application->created_at}}</li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection