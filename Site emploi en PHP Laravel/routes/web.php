<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Page d'acceuil
Route::get('/',function (){
    $offers = \App\Offer::all();
    $applications = \App\Application::all();
   return view('home',compact('offers','applications'));
});
Route::get('/needconnect',function (){
    return view('needconnect');
});
Route::get('/cannotapply',function (){
    return view('cannotapply');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::namespace('Job')->prefix('job')->name('job.')->middleware('can:manage-users')->group(function (){
    Route::resource('/offers','OffersController',['except'=>'show'])->middleware('can:manage-users');
    Route::get('/offers/{offer}/candidats','ApplicationsController@CandidatsList')->middleware('can:manage-users');
});

Route::namespace('Job')->prefix('job')->name('job.')->group(function (){
    Route::resource('/applications','ApplicationsController',['except'=>'show']);
});


Route::get('/logged', 'LoggedController@index');

Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('can:manage-users')->group(function (){
    Route::resource('/users','UsersController',['except'=>['show','create','store']])->middleware('can:manage-users');

});


