<?php

use Illuminate\Database\Seeder;
use App\Application;
use App\User;
use App\Offer;

class ApplicationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Application::truncate();
        DB::table('application_user')->truncate();
        DB::table('application_offer')->truncate();

        $offer1App = Offer::where('poste','Développeur java/ Reactjs / Freelance')->first();
        $offer2App = Offer::where('poste','Boulanger')->first();
        $offer3App = Offer::where('poste','Plombier')->first();
        $adminApp = User::where('prenom','Super')->first();
        $candidatApp = User::where('prenom','candidat')->first();



        $app1 = Application::create(['id'=>'1']);
        $app2 = Application::create(['id'=>'2']);
        $app3 = Application::create(['id'=>'3']);


        $app1->users()->attach($adminApp);
        $app1->offers()->attach($offer2App);
        $app2->users()->attach($candidatApp);
        $app2->offers()->attach($offer1App);
        $app3->users()->attach($candidatApp);
        $app3->offers()->attach($offer3App);






    }
}
