<?php

use Illuminate\Database\Seeder;
use App\Offer;
use App\User;
class OffersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Offer::truncate();

        \Illuminate\Support\Facades\DB::table('offer_user')->truncate();

        $offerAdmin = User::where('prenom','Super')->first();
        $offerEmployeur = User::where('prenom','Employeur')->first();


        $offer1 = Offer::create([
            'poste'=>'Développeur java/ Reactjs / Freelance',
            'entreprise'=>'CS Group Solutions',
            'ville_ent'=>'Paris',
            'departement'=>'75',
            'type_contrat'=>'CDI',
            'desc'=>'Longue description',
            'remuneration'=>'50k € par ans',

        ]);
        $offer2 = Offer::create([
            'poste'=>'Boulanger',
            'entreprise'=>'TestCorp',
            'ville_ent'=>'Paris',
            'departement'=>'75',
            'type_contrat'=>'CDI',
            'desc'=>'Longue description',
            'remuneration'=>'50k € par ans',

        ]);
        $offer3 = Offer::create([
            'poste'=>'Plombier',
            'entreprise'=>'TestCorp',
            'ville_ent'=>'Paris',
            'departement'=>'75',
            'type_contrat'=>'CDI',
            'desc'=>'Longue description',
            'remuneration'=>'50k € par ans',

        ]);
        $offer1->users()->attach($offerAdmin);
        $offer2->users()->attach($offerEmployeur);
        $offer3->users()->attach($offerEmployeur);


    }
}
