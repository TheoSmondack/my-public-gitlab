<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        \Illuminate\Support\Facades\DB::table('role_user')->truncate();


        $adminRole = Role::where('name','admin')->first();
        $employeurRole = Role::where('name','employeur')->first();
        $candidatRole = Role::where('name','candidat')->first();

        $admin = User::create([
            'name'=>'User',
            'prenom'=>'Super',
            'email'=>'superuser@user.com',
            'civi'=>'Monsieur',
            'adresse'=>'24 rue de la rue',
            'ville'=>'Paris',
            'codep'=>'75017',
            'dateNaiss'=>'1997-07-04',
            'password'=> Hash::make('superpass123'),
        ]);
        $employeur = User::create([
            'name'=>'User',
            'prenom'=>'Employeur',
            'email'=>'employeuruser@user.com',
            'civi'=>'Monsieur',
            'adresse'=>'24 rue de la rue',
            'ville'=>'Paris',
            'codep'=>'75017',
            'dateNaiss'=>'1997-07-04',
            'password'=> Hash::make('123456789'),
        ]);
        $candidat = User::create([
            'name'=>'User',
            'prenom'=>'candidat',
            'email'=>'candidatuser@user.com',
            'civi'=>'Monsieur',
            'adresse'=>'24 rue de la rue',
            'ville'=>'Paris',
            'codep'=>'75017',
            'dateNaiss'=>'1997-07-04',
            'password'=> Hash::make('123456789'),
        ]);
        $admin->roles()->attach($adminRole);
        $employeur->roles()->attach($employeurRole);
        $candidat->roles()->attach($candidatRole);

    }
}
