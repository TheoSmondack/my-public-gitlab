﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="Projet_5.Register" %>

<%@ Import Namespace="System.Collections.Generic" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="Register.css" />

    <title>Paris Foot</title>
</head>

<body>
    <div class="whiteBack">
        <div class="register-form">
            <h2 style="text-align: center;">Enregistrez-vous</h2>
            <br />
            <form id="form2" runat="server">
                <div class="form-row input-margin">
                    <div class="col">
                        <label for="nom">Nom</label>
                        <asp:TextBox ID="nom" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col">
                        <label for="prenom">Prénom</label>
                        <asp:TextBox ID="prenom" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-row input-margin">
                    <div class="col">
                        <label for="email">Email</label>
                        <asp:TextBox TextMode="Email" ID="email" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <label for="adress">Adresse</label>
                        <asp:TextBox ID="adresse" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="city">Ville</label>
                        <asp:TextBox ID="ville" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="code_p">Code Postal</label>
                        <asp:TextBox ID="codep" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-row input-margin">
                    <div class="col">
                        <label for="password">Mot de passe</label>
                        <asp:TextBox TextMode="Password" ID="pass" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col">
                        <label for="pass_verif">Retaper mot de passe</label>
                        <asp:TextBox TextMode="Password" ID="pass_verif" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-row input-margin">
                    <div class="col">
                        <label for="dateNaiss">Date de naissance</label>
                        <asp:TextBox TextMode="Date" ID="date_naiss" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col">
                        <label for="team">Votre équipe</label>
                        <asp:DropDownList runat="server" ID="equipe" CssClass="form-control">
                            <asp:ListItem Text="Choisissez votre équipe" Value="0"/>
                            <asp:ListItem Text="1 er arrondissement" Value="1"/>
                            <asp:ListItem Text="2 eme arrondissement" Value="2" />
                            <asp:ListItem Text="3 eme arrondissement" Value="3" />
                            <asp:ListItem Text="4 eme arrondissement" Value="4" />
                            <asp:ListItem Text="5 eme arrondissement" Value="5" />
                            <asp:ListItem Text="6 eme arrondissement" Value="6" />
                            <asp:ListItem Text="7 eme arrondissement" Value="7" />
                            <asp:ListItem Text="8 eme arrondissement" Value="8" />
                            <asp:ListItem Text="9 eme arrondissement" Value="9" />
                            <asp:ListItem Text="10 eme arrondissement" Value="10" />
                            <asp:ListItem Text="11 eme arrondissement" Value="11" />
                            <asp:ListItem Text="12 eme arrondissement" Value="12" />
                            <asp:ListItem Text="13 eme arrondissement" Value="13" />
                            <asp:ListItem Text="14 eme arrondissement" Value="14" />
                            <asp:ListItem Text="15 eme arrondissement" Value="15" />
                            <asp:ListItem Text="16 eme arrondissement" Value="16" />
                            <asp:ListItem Text="17 eme arrondissement" Value="17" />
                            <asp:ListItem Text="18 eme arrondissement" Value="18" />
                            <asp:ListItem Text="19 eme arrondissement" Value="19" />
                            <asp:ListItem Text="20 eme arrondissement" Value="20" />
                        </asp:DropDownList>
                    </div>
                </div>

                <div class="col-sm-12 text-center">
                    <asp:Button ID="registerButton" Text="S'enregistrer" CssClass="btn btn-primary btn-md center-block logSubmit" onclick="register_User" runat="server"/>
                    <small id="registerHelp" class="form-text text-muted">Déjà inscrit ? <a href="Login.aspx">Connectez-vous ici</a></small>
                    <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                </div>
            </form>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
