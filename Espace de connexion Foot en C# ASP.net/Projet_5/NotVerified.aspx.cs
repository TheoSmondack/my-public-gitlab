﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Projet_5
{
    public partial class NotVerified : System.Web.UI.Page
    {
        public int verif;
        protected void Page_Load(object sender, EventArgs e)
        {
            verif = Convert.ToInt32(Session["verif"]);
            if ((verif == 1) && (Session["email"]!=null))
            {
                Response.Redirect("YourDashboard.aspx");
            }
            if (Session["email"] == null)
            {
                Response.Redirect("Login.aspx");
            }
        }
        protected void disconnect_User(object sender, EventArgs e)
        {
            Response.Redirect("Logout.aspx");
        }
    }
}