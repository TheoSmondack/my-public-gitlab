﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
namespace Projet_5
{
    public partial class AdminDashboard : System.Web.UI.Page
    {
        public string user_type;
        public int verif;

        protected void Page_Load(object sender, EventArgs e)
        {
            verif = Convert.ToInt32(Session["verif"]);
            if (verif == 0)
            {
                Response.Redirect("NotVerified.aspx");
            }
            if (Session["email"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            switch (Session["user_type"])
            {
                case 1:
                case 2:
                    Response.Redirect("YourDashboard.aspx");
                    break;
                case 3:
                    this.user_type = "administrateur";
                    break;

            }

            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();

            string connectionString = @"Data Source=localhost;port=3306;Initial Catalog=paris_sport;User Id=root;password=''";
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    connection.Open();
                    cmd.Connection = connection;

                    cmd.CommandText = "select * from users where verif=0;";
                    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                    da.Fill(dt);
                    connection.Close();
                }
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    connection.Open();
                    cmd.Connection = connection;

                    cmd.CommandText = "select * from users;";
                    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                    da.Fill(dt2);
                    connection.Close();
                }
            }


            StringBuilder sb = new StringBuilder();
            StringBuilder sb1 = new StringBuilder();

            sb.Append(@"
                    <table border='1'>
                    <tr>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Equipe</th>
                    <th>Type d'utilisateur</th>
                    <th>Status</th>
                    <th>Actions</th>
                    </tr>
                    ");

            if (dt.Rows.Count == 0)
            {
                sb.Append("<tr><td colspan='4'>Pas de données</td></tr>");
            }
            else
            {
                foreach (DataRow dr in dt.Rows)
                {

                    sb.Append("<tr>");

                    sb.AppendFormat("<td>{0}</td>", dr["nom"]);
                    sb.AppendFormat("<td>{0}</td>", dr["prenom"]);
                    sb.AppendFormat("<td>{0}</td>", dr["id_equipe"] + " e arrondissement");
                    switch (dr["user_type"])
                    {
                        case 1:
                            sb.AppendFormat("<td>{0}</td>", "Joueur");
                            break;
                        case 2:
                            sb.AppendFormat("<td>{0}</td>", "Entraineur");
                            break;
                       
                    }
                    switch (dr["verif"])
                    {
                        case 0:
                            sb.AppendFormat("<td>{0}</td>", "Non validé");
                            break;
                        case 1:
                            sb.AppendFormat("<td>{0}</td>", "Validé");
                            break;
                        case 3:
                            sb.AppendFormat("<td>{0}</td>", "Banni");
                            break;

                    }

                    sb.AppendFormat("<td>{0}</td>", "<a href='EditUser.aspx'>Editer</a>");
                    sb.Append("</tr>");
                }

            }
          
            sb.Append("</table>");
            sb1.Append(@"
                    <table border='1'>
                    <tr>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Equipe</th>
                    <th>Type d'utilisateur</th>
                    <th>Status</th>
                    <th>Actions</th>    
                    </tr>
                    ");

            if (dt2.Rows.Count == 0)
            {
                sb1.Append("<tr><td colspan='4'>Pas de données</td></tr>");
            }
            else
            {
                foreach (DataRow dr in dt2.Rows)
                {

                    sb1.Append("<tr>");

                    sb1.AppendFormat("<td>{0}</td>", dr["nom"]);
                    sb1.AppendFormat("<td>{0}</td>", dr["prenom"]);
                    sb1.AppendFormat("<td>{0}</td>", dr["id_equipe"] + " e arrondissement");
                    switch (dr["user_type"])
                    {
                        case 1:
                            sb1.AppendFormat("<td>{0}</td>", "Joueur");
                            break;
                        case 2:
                            sb1.AppendFormat("<td>{0}</td>", "Entraineur");
                            break;
                        case 3:
                            sb1.AppendFormat("<td>{0}</td>", "Administrateur");
                            break;
                    }


                    switch (dr["verif"])
                    {
                        case 0:
                            sb1.AppendFormat("<td>{0}</td>", "Non validé");
                            break;
                        case 1:
                            sb1.AppendFormat("<td>{0}</td>", "Validé");
                            break;
                        case 3:
                            sb1.AppendFormat("<td>{0}</td>", "Banni");
                            break;

                    }

                    sb1.AppendFormat("<td>{0}</td>", "<a href='EditUser.aspx'>Editer</a>");
                    sb1.Append("</tr>");
                }

            }
            sb1.Append("</table>");
            ph1.Controls.Add(new LiteralControl(sb.ToString()));
            ph2.Controls.Add(new LiteralControl(sb1.ToString()));

        }



    }
}
