﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Projet_5.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
     <!-- Required meta tags -->
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"/>
    <link rel="stylesheet" href="Login.css" />

    <title>Paris Foot</title>
  </head>

<body>
    <div class="whiteBack">
        <div class="login-form">
            <h2 style="text-align:center;">Connectez-vous</h2><br />
            <form id="form1" runat="server">
                <div class="form-group">
                    <label for="emailLogin">Identifiant (email)</label>
                    <asp:TextBox TextMode="Email" ID="login" CssClass="form-control"  runat="server"></asp:TextBox>
                    <!--<input type="email" name="login" class="form-control" id="login"/>-->
                </div>
                <div class="form-group">
                    <label for="passwordLogin">Mot de passe</label>
                    <asp:TextBox TextMode="Password" ID="pass" CssClass="form-control" runat="server"></asp:TextBox>
                    <!--<input type="password" name="pass" class="form-control" id="pass"/>-->
                </div>
                <div class="col-sm-12 text-center">
                    <asp:Button ID="loginButton" Text="Connexion" CssClass="btn btn-primary btn-md center-block logSubmit" onclick="connect_User" runat="server"/>
                    <!--<button type="submit" id="loginButton" class="btn btn-primary btn-md center-block logSubmit"aria-describedby="registerHelp">Connexion</button>-->
                    <small id="registerHelp" class="form-text text-muted">Pas encore inscrit ? <a href="Register.aspx">Inscrivez-vous ici</a></small>
                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                </div>
            </form>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
