﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Data;

namespace Projet_5
{
    public partial class Register : System.Web.UI.Page
    {
        // MySqlConnection con = new MySqlConnection(@"Data Source=localhost;port=3306;Initial Catalog=paris_sport;User Id=root;password=''");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["email"] != null)
            {
                Response.Redirect("YourDashboard.aspx");
            }

        }
        protected void register_User(object sender, EventArgs e)
        {
            

            string connectionString = @"Data Source=localhost;port=3306;Initial Catalog=paris_sport;User Id=root;password=''";
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                if (pass.Text != pass_verif.Text)
                {
                    Label2.Text = "Les mots de passes ne sont pas identiques";

                }
                else if (equipe.Text == "0")
                {
                    Label2.Text = "Veuillez choisir votre équipe";
                }
                else
                {
                    try
                    {
                        string insertData = "insert into users(nom,prenom,email,adresse,ville,code_p,password,date_naiss,id_equipe)" +
                        "values(@nom, @prenom, @email, @adresse, @ville, @code_p, @password, @date_naiss, @id_equipe); ";
                        MySqlCommand command = new MySqlCommand(insertData, connection);

                        command.Parameters.AddWithValue("@nom", nom.Text);
                        command.Parameters.AddWithValue("@prenom", prenom.Text);
                        command.Parameters.AddWithValue("@email", email.Text);
                        command.Parameters.AddWithValue("@adresse", adresse.Text);
                        command.Parameters.AddWithValue("@ville", ville.Text);
                        command.Parameters.AddWithValue("@code_p", codep.Text);
                        command.Parameters.AddWithValue("@password", pass.Text);
                        command.Parameters.AddWithValue("@date_naiss", date_naiss.Text);
                        command.Parameters.AddWithValue("@id_equipe", equipe.Text);



                        connection.Open();
                        int result = command.ExecuteNonQuery();
                        Label2.Text = "Utilisateur enregistré avec succès";






                    }
                    catch (Exception ex)
                    {
                        Label2.Text = "Vous n'avez pas remplis tout les champs";

                    }
                }
            }



        }

    }
}