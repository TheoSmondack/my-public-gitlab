﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Projet_5
{
    public partial class YourDashboard : System.Web.UI.Page
    {
        public string user_type;
        public int verif;

        protected void Page_Load(object sender, EventArgs e)
        {
            verif = Convert.ToInt32(Session["verif"]);
            if (verif == 0)
            {
                Response.Redirect("NotVerified.aspx");
            }
            if(Session["email"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            switch (Session["user_type"])
            {
                case 1:
                    this.user_type = "joueur";
                    break;
                case 2:
                    this.user_type = "entraineur";
                    break;
                case 3:
                    this.user_type = "administrateur";
                 break;

            }
            


        }
        protected void disconnect_User(object sender, EventArgs e)
        {
            Response.Redirect("Logout.aspx");
        }
    }
}