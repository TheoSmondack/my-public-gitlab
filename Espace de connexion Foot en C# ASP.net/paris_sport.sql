-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 18 mai 2020 à 12:48
-- Version du serveur :  8.0.19
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `paris_sport`
--

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) DEFAULT NULL,
  `prenom` varchar(45) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `ville` varchar(255) DEFAULT NULL,
  `code_p` varchar(5) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `date_naiss` date DEFAULT NULL,
  `id_equipe` int DEFAULT NULL,
  `user_type` int DEFAULT '1',
  `verif` int DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `nom`, `prenom`, `email`, `adresse`, `ville`, `code_p`, `password`, `date_naiss`, `id_equipe`, `user_type`, `verif`) VALUES
(1, 'User', 'Super', 'admin@foot.com', '6-8 Impasse des 2 Cousins', 'Paris', '75017', 'superpass123', '1997-07-04', 0, 3, 1),
(2, 'Mourinho', 'José', 'j.mourinho@foot.com', '6-8 Impasse des 2 Cousins', 'Paris', '75017', '123456789', '1963-01-26', 16, 2, 1),
(3, 'Kane', 'Harry', 'h.kane@foot.com', '6-8 Impasse des 2 Cousins', 'Paris', '75017', '123456789', '1993-07-28', 16, 1, 1),
(4, 'Messi', 'Lionel', 'l.messi@foot.com', '2 rue de la rue', 'Paris', '75010', '123456789', '1987-06-24', 17, 1, 0),
(5, 'test', 'test', 'test@foot.com', '4 rue test', 'Paris', '75001', 'azertyuiop', '1998-05-13', 10, 2, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
