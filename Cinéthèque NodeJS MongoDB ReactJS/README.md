# Projet 
Ce projet est une cinémathèque qui propose les fonctionnalités suivantes:
- Ajout de réalisateurs.
- Ajout de films avec une relation au réalisateur correspondant enregistré par l'utilisateur.
- Affichage de la liste des films avec des fonctionnalités de modification et suppression des films.

Les données sont stockés sur une base de données NoSQL MongoDB.

## Pré-requis
Il est necessaire d'avoir [NodeJS](https://nodejs.org/en/) d'installer sur votre machine pour deployer ce projet

## Deploiement

Ouvrir un invite de commande de votre ordinateur ou bien de IDE et entrer les commandes suivantes:

Pour lancer le serveur:
```bash
cd mongo-db-cinema
cd backend
nodemon server
```
Pour lancer le site (partie client):
```bash
cd mongo-db-cinema 
npm start
```

## Accès
Une fois ces commandes éxécutés vous pouvez accéder au site à l'addresse suivante:

```python
http://localhost:3000/
```
