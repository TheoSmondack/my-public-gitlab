const router = require('express').Router();
let Realisator = require('../models/realisator.model');

router.route('/').get((req,res) => {
    Realisator.find()
        .then(actors => res.json(actors))
        .catch(err => res.status(400).json('Error: '+err));
});

router.route('/add').post((req,res) => {
    const firstname = req.body.firstname;
    const lastname = req.body.lastname;
    const nationality = req.body.nationality;
    const birthdate = Date.parse(req.body.birthdate);

    const newRealisator = new Realisator({
        firstname,
        lastname,
        nationality,
        birthdate
    });

    newRealisator.save()
        .then(() => res.json('Réalisateur ajouté !'))
        .catch(err => res.status(400).json('Error: '+err));

});

module.exports = router;