const router = require('express').Router();
let Movie = require('../models/movie.model');

router.route('/').get((req,res) => {
    Movie.find()
        .then(movies => res.json(movies))
        .catch(err => res.status(400).json('Error: '+err));
});

router.route('/add').post((req,res) => {
    const title = req.body.title;
    const type = req.body.type;
    const releasedate = Date.parse(req.body.releasedate);
    const duration = Number(req.body.duration);
    const  realisator = req.body.realisator;

    const newMovie = new Movie({
        title,
        type,
        releasedate,
        duration,
        realisator
    })

    newMovie.save()
        .then(() => res.json('Film ajouté !'))
        .catch(err => res.status(400).json('Error: '+err));

});
router.route('/:id').get((req,res) => {
    Movie.findById(req.params.id)
        .then(movie => res.json(movie))
        .catch(err => res.status(400).json('Error: '+err));
});
router.route('/:id').delete((req,res) => {
    Movie.findByIdAndDelete(req.params.id)
        .then(() => res.json('Film supprimé'))
        .catch(err => res.status(400).json('Error: '+err));
});
router.route('/update/:id').post((req,res) => {
    Movie.findById(req.params.id)
        .then(movie => {
            movie.title = req.body.title;
            movie.type = req.body.type;
            movie.releasedate = Date.parse(req.body.releasedate);
            movie.duration = Number(req.body.duration);
            movie.realisator=req.body.realisator;

            movie.save()
                .then(()=>res.json('Film modifié avec succès !'))
                .catch(err => res.status(400).json('Error: '+err));


        })
        .catch(err => res.status(400).json('Error: '+err));
});

module.exports = router;