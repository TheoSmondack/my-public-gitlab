const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const realisatorSchema = new Schema({
    firstname:{
        type:String,
        required: true,
    },
    lastname:{
        type:String,
        required: true,
    },
    nationality:{
        type:String,
        required: true,
    },
    birthdate:{
        type:Date,
        required: true,
    },
},{
    timestamp:true,
});

const Realisator = mongoose.model('Realisator',realisatorSchema);

module.exports = Realisator;