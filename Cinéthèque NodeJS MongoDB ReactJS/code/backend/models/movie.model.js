const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const movieSchema = new Schema({
    title:{
        type:String,
        required: true,
    },
    type:{
        type:String,
        required: true,
    },

    releasedate:{
        type:Date,
        required: true,
    },
    duration:{
        type:Number,
        required: true,
    },
    realisator:{
        type:String,
        required:true,
    }
},{
    timestamp:true,
});

const Movie = mongoose.model('Movie',movieSchema);

module.exports = Movie;