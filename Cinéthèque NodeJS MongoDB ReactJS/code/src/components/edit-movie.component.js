import React,{Component} from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import axios from "axios";
export default class EditMovie extends Component{
    constructor(props) {
        super(props);

        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangeType = this.onChangeType.bind(this);
        this.onChangeReleaseDate = this.onChangeReleaseDate.bind(this);
        this.onChangeDuration = this.onChangeDuration.bind(this);
        this.onChangeRealisator = this.onChangeRealisator.bind(this);
        this.onSubmit = this.onSubmit.bind(this);


        this.state = {
            title:'',
            type:'',
            releasedate:new Date(),
            duration:0,
            realisator:'',
            realisators:[]
        }
    }
    componentDidMount() {
        axios.get('http://localhost:5000/movies/'+this.props.match.params.id)
            .then(response => {
                this.setState({
                    title: response.data.title,
                    type: response.data.type,
                    releasedate: new Date(response.data.releasedate),
                    duration: response.data.duration,
                    realisator: response.data.realisator
                })
            })
            .catch(function (error) {
                console.log(error);
            })
        axios.get('http://localhost:5000/realisators/')
            .then(response=>{
                if(response.data.length >0){
                    this.setState({
                        realisators:response.data.map(realisator=>realisator.firstname+" "+realisator.lastname),
                    })
                }
            })
            .catch((error)=>{
                console.log(error);
            })

    }

    onChangeTitle(e){
        this.setState({
            title:e.target.value
        });
    }
    onChangeType(e){
        this.setState({
            type:e.target.value
        });
    }
    onChangeReleaseDate(date){
        this.setState({
            releasedate:date
        });
    }
    onChangeDuration(e){
        this.setState({
            duration:e.target.value
        });
    }
    onChangeRealisator(e){
        this.setState({
            realisator:e.target.value
        });
    }
    onSubmit(e){
        e.preventDefault();

        const movie = {
            title: this.state.title,
            type: this.state.type,
            releasedate: this.state.releasedate,
            duration: this.state.duration,
            realisator: this.state.realisator
        }
        console.log(movie);
        axios.post('http://localhost:5000/movies/update/'+this.props.match.params.id,movie)
            .then(res=>console.log(res.data));

        window.location='/';
    }

    render() {
        return(
            <div>
                <h3>Modifier un film</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Titre: </label>
                        <input type="text"
                               required
                               className="form-control"
                               value={this.state.title}
                               onChange={this.onChangeTitle}
                        />
                    </div>
                    <div className="form-group">
                        <label>Réalisateur: </label>
                        <select ref="realisatorInput"
                                required
                                className="form-control"
                                value={this.state.realisator}
                                onChange={this.onChangeRealisator}>
                            {
                                this.state.realisators.map(function (realisator) {
                                    return<option
                                        key={realisator}
                                        value={realisator}>{realisator}
                                    </option>;
                                })
                            }
                        </select>
                    </div>
                    <div className="form-group">
                        <label>Type: </label>
                        <input type="text"
                               required
                               className="form-control"
                               value={this.state.type}
                               onChange={this.onChangeType}
                        />
                    </div>
                    <div className="form-group">
                        <label>Date de sortie: </label>
                        <div>
                            <DatePicker
                                selected={this.state.releasedate}
                                onChange={this.onChangeReleaseDate}
                            />
                        </div>
                    </div>
                    <div className="form-group">
                        <label>Durée du film (en minutes): </label>
                        <input type="text"
                               required
                               className="form-control"
                               value={this.state.duration}
                               onChange={this.onChangeDuration}
                        />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Modifier le film" className="btn btn-primary"/>
                    </div>


                </form>
            </div>
        )
    }
}