import React,{Component} from "react";
import {Link} from 'react-router-dom';
import axios from 'axios';
const Movie = props =>(
    <tr>
        <td>{props.movie.title}</td>
        <td>{props.movie.type}</td>
        <td>{props.movie.releasedate.substring(0,10)}</td>
        <td>{props.movie.duration} min</td>
        <td>{props.movie.realisator}</td>
        <td>
            <Link to={"/edit/"+props.movie._id}>Modifier</Link> | <a href="#" onClick={() => { props.deleteMovie(props.movie._id) }}>Supprimer</a>
        </td>
    </tr>
)

export default class MoviesList extends Component{
    constructor(props) {
        super(props);
        this.deleteMovie = this.deleteMovie.bind(this);

        this.state = {movies: []};
    }
    componentDidMount(){
        axios.get('http://localhost:5000/movies')
            .then(response=>{
                this.setState({movies: response.data})
            })
            .catch((error) => {
                console.log(error);
            })
    }
    deleteMovie(id){
        axios.delete('http://localhost:5000/movies/'+id)
            .then(response=>console.log(response.data));
        this.setState({
            movies:this.state.movies.filter(el => el._id !== id)
        })
    }
    movieList() {
        return this.state.movies.map(currentmovie => {
            return <Movie movie={currentmovie} deleteMovie={this.deleteMovie} key={currentmovie._id}/>;
        })
    }




    render() {
        return(
            <div>
                <h3>Liste des films</h3>
                <table className="table">
                    <thead className="thead-light">
                    <tr>
                        <th>Titre</th>
                        <th>Type</th>
                        <th>Date de sortie</th>
                        <th>Durée</th>
                        <th>Réalisateur</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.movieList()}
                    </tbody>
                </table>
            </div>
        )
    }
}