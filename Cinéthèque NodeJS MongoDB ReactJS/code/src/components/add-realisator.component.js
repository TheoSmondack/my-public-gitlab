import React,{Component} from "react";
import DatePicker from "react-datepicker";
import axios from "axios";
export default class AddRealisator extends Component{
    constructor(props) {
        super(props);

        this.onChangeFisrtName = this.onChangeFisrtName.bind(this);
        this.onChangeLastName = this.onChangeLastName.bind(this);
        this.onChangeNationality = this.onChangeNationality.bind(this);
        this.onChangeBirthDate = this.onChangeBirthDate.bind(this);
        this.onSubmit = this.onSubmit.bind(this);


        this.state = {
            firstname:'',
            lastname:'',
            nationality:'',
            birthdate:new Date(),
        }
    }
    onChangeFisrtName(e){
        this.setState({
            firstname:e.target.value
        });
    }
    onChangeLastName(e){
        this.setState({
            lastname:e.target.value
        });
    }
    onChangeNationality(e){
        this.setState({
            nationality:e.target.value
        });
    }
    onChangeBirthDate(date){
        this.setState({
            birthdate:date
        });
    }
    onSubmit(e){
        e.preventDefault();

        const realisator = {
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            nationality: this.state.nationality,
            birthdate: this.state.birthdate
        }
        console.log(realisator);
        axios.post('http://localhost:5000/realisators/add',realisator)
            .then(res => console.log(res.data));

        this.setState({
                firstname:'',
                lastname:'',
                nationality:'',
                birthdate:new Date(),
        })
    }
    render() {
        return(
            <div>
                <h3>Ajouter un réalisateur</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Prénom: </label>
                        <input type="text"
                               required
                               className="form-control"
                               value={this.state.firstname}
                               onChange={this.onChangeFisrtName}
                        />
                    </div>
                    <div className="form-group">
                        <label>Nom: </label>
                        <input type="text"
                               required
                               className="form-control"
                               value={this.state.lastname}
                               onChange={this.onChangeLastName}
                        />
                    </div>
                    <div className="form-group">
                        <label>Nationalité: </label>
                        <input type="text"
                               required
                               className="form-control"
                               value={this.state.nationality}
                               onChange={this.onChangeNationality}
                        />
                    </div>

                    <div className="form-group">
                        <label>Date de naissance: </label>
                        <div>
                            <DatePicker
                                selected={this.state.birthdate}
                                onChange={this.onChangeBirthDate}
                            />
                        </div>
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Ajouter un réalisateur" className="btn btn-primary"/>
                    </div>
                </form>
            </div>
        )
    }
}