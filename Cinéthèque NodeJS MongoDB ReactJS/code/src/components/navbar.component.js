import React,{Component} from "react";
import {Link} from 'react-router-dom';

export default  class Navbar extends Component{
    render() {
        return(
            <nav className="navbar navbar-dark bg-dark navbar-expand-lg">
                <Link to="/" className="navbar-brand">Films</Link>
                <div className="collapse navbar-collapse">
                    <ul className="navbar-nav mr-auto">
                        <li className="navbar-item">
                            <Link to="/" className="nav-link">Films</Link>
                        </li>
                        <li className="navbar-item">
                            <Link to="/addmovie" className="nav-link">Ajouter un film</Link>
                        </li>
                        <li className="navbar-item">
                            <Link to="/addrealisator" className="nav-link">Ajouter un réalisateur</Link>
                        </li>
                    </ul>
                </div>
            </nav>
        )
    }
}