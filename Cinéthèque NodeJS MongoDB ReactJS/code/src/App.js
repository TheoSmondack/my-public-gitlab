import React from 'react';
import {BrowserRouter as Router, Route} from "react-router-dom";
import  "bootstrap/dist/css/bootstrap.min.css"

import Navbar from "./components/navbar.component";
import MoviesList from "./components/movies-list.component";
import EditMovie from "./components/edit-movie.component";
import AddMovie from "./components/add-movie.component";
import AddRealisator from "./components/add-realisator.component";



function App() {
  return (
      <Router>
          <div className="container">
              <Navbar />
          <br/>
              <Route path="/" exact component={MoviesList}/>
              <Route path="/edit/:id" exact component={EditMovie}/>
              <Route path="/addmovie" exact component={AddMovie}/>
              <Route path="/addrealisator" exact component={AddRealisator} />
          </div>
      </Router>
  );
}

export default App;
